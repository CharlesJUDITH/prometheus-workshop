# Pushgateway

The Pushgateway is an intermediary service which allows you to push metrics from jobs which cannot be scraped. For details on this exporter check the [prometheus website](https://prometheus.io/docs/practices/pushing/).

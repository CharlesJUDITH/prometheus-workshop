# Alerting

```
Expected results:
- Being able to view your alerts
- Being able to create alerting rules
```

## Start Alertmanager

`scripts/test-alertmanager.sh configs/alertmanager-workshop.yml`

Check the web interface on your browser: http://localhost:9093

Create your own rules for 2 metrics (in configs/myrules.rules), save your changes and restart the prometheus instance.

## Test your alerting configuration

`./amtool check-config configs/alertmanager*.yml`

## Dashboard for alertmanager

[karma](https://github.com/prymitive/karma) is a dashboard for the alertmanager.

## Debugging the alertmanager

[alertmanager2es](https://github.com/webdevops/alertmanager2es) is very useful to debug some issues iwth the the alertmanager. For example if you want to investigate on "missing" notifications.

[The alertmanager routing tree editor](https://www.prometheus.io/webtools/alerting/routing-tree-editor/) is a good way to test a part of your alerting configuration. This could be done with the AM (Alertmanager) CLI.

# Querying Prometheus

## Play with the metrics

The rate() function

`rate(prometheus_http_request_duration_seconds_count[5m])`

The sum aggregation operator

`sum(rate(prometheus_http_request_duration_seconds_count[5m]))`

Select by label

`prometheus_target_interval_length_seconds{quantile="0.99"}`

Aggregate by label

`sum(rate(prometheus_http_request_duration_seconds_count[5m])) by (job)`

`sum(rate(prometheus_http_request_duration_econds_count[5m])) by (job, instance)`

Arithmetic

`rate(prometheus_http_request_duration_seconds_sum[5m]) / rate(prometheus_http_request_duration_seconds_count[5m])`

`sum(rate(prometheus_http_request_duration_seconds_sum[5m])) by (job) / sum(rate(prometheus_http_request_duration_seconds_count[5m])) by (job)`

Other metrics
```
prometheus_target_interval_length_seconds
prometheus_target_interval_length_seconds{quantile="0.99"}
prometheus_target_interval_length_seconds_count
up
```

All the functions are documented on the [Prometheus website](https://prometheus.io/docs/querying/functions/)

[PromQL cheat sheet](https://promlabs.com/promql-cheat-sheet/)

# Exporters

There is a list of exporters on the [prometheus website](https://prometheus.io/docs/instrumenting/exporters/)

In this workshop we'll see the node and the blackbox exporters.

## Node exporter

This is an exporter for hardware and OS metrics exposed by *NIX kernels.

Launch the node exporter:
`scripts/node_exporter.sh`

Check the metrics on your browser:

http://localhost:9100/metrics

## Blackbox exporter

This exporter allows blackbox probing of endpoints over HTTP, HTTPS, DNS, TCP and ICMP.

Run Blackbox exporter:

`cd blackbox_exporter`

### Option 1

TODO

### Option 2

This option require Docker

`docker run --rm -d -p 9115:9115 --name blackbox_exporter -v pwd:/config prom/blackbox-exporter:master --config.file=/config/blackbox.yml`

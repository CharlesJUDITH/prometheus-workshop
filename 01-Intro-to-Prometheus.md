# Intro to Prometheus

## Architecture
![](https://prometheus.io/assets/architecture.png)

## Start Prometheus

`scripts/test-prometheus.sh configs/prometheus-workshop.yml`

Connect on the web interface from your browser: 

`http://localhost:9090`

Expected results:
- Being able to see the web interface
- Discover web interface

## Test your config


- Prometheus configuration

`./promtool check config configs/prometheus-workshop.yml`

- Rules 

`./promtool check rules configs/*.rules.yml`

Check metric consistency: 

`curl -s http://localhost:9100/metrics | ./promtool check metrics`

## Recording rules

Uncomment the `rules_files` parameter and value in the prometheus configuration: `configs/prometheus-workshop.yml`

Stop the current Prometheus instance, save your changes and start it again.

Check the rules in the Prometheus web interface: 

`http://localhost:9090/rules`

Create your own rules for 2 metrics (in configs/myrules), save your changes and restart the prometheus instance.

Stop the current Prometheus instance, save your changes and start it again.

Check the rules in the Prometheus web interface: 

`http://localhost:9090/rules`
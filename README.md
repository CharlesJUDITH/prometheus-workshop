# Prometheus 101 workshop

This workshop was tested on Linux and OSX but not on Windows. If you're on Windows, try to run the code on a Linux VM.


## [Intro to Prometheus](01-Intro-to-Prometheus.md)

## [Querying Prometheus](02-Querying-Prometheus.md)

## [Exporters](03-Exporters.md)

## [Alerting](04-Alerting.md)

## [Pushgateway](05-Pushgateway.md)

## [Instrumenting code](06-Instrumenting-Code.md)

## [Dashboarding with Grafana](07-Dashboards-with-Grafana.md)


## Additional resources

- [Prometheus Official documentation](https://prometheus.io/docs/introduction/overview/)
- [Robust perception blog](https://www.robustperception.io/blog)
- [Promlabs](https://promlabs.com/blog/)

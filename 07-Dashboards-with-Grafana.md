# Dashboards with Grafana

## Option 1

Run `scripts/launch-grafana.sh`

## Option2 

For this option Docker is required.

`docker run -d -p 3000:3000 --name grafana grafana/grafana:latest`


Go on -> http://localhost:3000

Default login/password: admin admin

Add Prometheus Datasource
https://prometheus.io/docs/visualization/grafana/#creating-a-prometheus-data-source

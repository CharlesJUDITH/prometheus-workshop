#!/bin/sh

# Handling for the moment : darwin and linux
OPERATING_SYSTEM=$(uname -s | tr '[:upper:]' '[:lower:]')
GRAFANA_VERSION=7.3.5
GRAFANA_DIR=alertmanager-${GRAFANA_VERSION}.${OPERATING_SYSTEM}-amd64
GRAFANA_ARCHIVE=${GRAFANA_DIR}.tar.gz

if ! which wget; then
   echo "You need to install wget"
   exit 1
fi

if [ ! -f alertmanager ]; then
   wget https://dl.grafana.com/oss/release/grafana-${GRAFANA_DIR}
   tar -xf v${GRAFANA_DIR}
   cp ${GRAFANA_DIR}/bin/grafana-server grafana-server
   cp ${GRAFANA_DIR}/bin/grafana-cli grafana-cli
fi

DIR=$(mktemp -d)

./grafana-server

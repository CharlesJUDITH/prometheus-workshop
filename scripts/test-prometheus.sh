#!/bin/sh

if [ $# -lt 1 ]; then
    echo "Usage: $0 <config.yml> [tmpdir]"
    echo "Example: $0 ../foo/prometheus.yml [tmpdir]"
    exit 1
fi

# Handling for the moment : darwin and linux
OPERATING_SYSTEM=$(uname -s | tr '[:upper:]' '[:lower:]')
PROMETHEUS_VERSION=2.22.2
PROMETHEUS_DIR=prometheus-${PROMETHEUS_VERSION}.${OPERATING_SYSTEM}-amd64
PROMETHEUS_ARCHIVE=${PROMETHEUS_DIR}.tar.gz

if ! which wget; then
   echo "You need wget"
   exit 1
fi

if [ ! -f prometheus ]; then
   wget https://github.com/prometheus/prometheus/releases/download/v${PROMETHEUS_VERSION}/${PROMETHEUS_ARCHIVE}
   tar -xf ${PROMETHEUS_ARCHIVE}
   cp ${PROMETHEUS_DIR}/prometheus prometheus
   cp ${PROMETHEUS_DIR}/promtool promtool
fi

PROMETHEUS_YML=$(basename $1)
PROMETHEUS_CONFIG_DIR=$(dirname $1)

if [ $# = 2 ]; then
    DIR=$2
else
    DIR=$(mktemp -d)
fi

cp -v ${PROMETHEUS_CONFIG_DIR?}/* $DIR
mkdir $DIR/metrics

case "${PROMETHEUS_VERSION}" in
    2*)
        ./prometheus --storage.tsdb.path=${DIR}/metrics --config.file ${DIR}/${PROMETHEUS_YML} ;;
    *)
        ./prometheus -storage.local.path=${DIR}/metrics --config.file ${DIR}/${PROMETHEUS_YML} -alertmanager.url http://localhost:9093 ;;
esac

#!/bin/sh

# Handling for the moment : darwin and linux
OPERATING_SYSTEM=$(uname -s | tr '[:upper:]' '[:lower:]')
NODE_EXPORTER_VERSION=1.0.1
NODE_EXPORTER_DIR=node_exporter-${NODE_EXPORTER_VERSION}.${OPERATING_SYSTEM}-amd64
NODE_EXPORTER_ARCHIVE=${NODE_EXPORTER_DIR}.tar.gz

if ! which wget; then
   echo "You need wget"
   exit 1
fi

if [ ! -f node_exporter ]; then
   wget https://github.com/prometheus/node_exporter/releases/download/v${NODE_EXPORTER_VERSION}/${NODE_EXPORTER_ARCHIVE}
   tar -xf ${NODE_EXPORTER_ARCHIVE}
   cp ${NODE_EXPORTER_DIR}/node_exporter node_exporter
fi

./node_exporter

